/*
  Modbus-Arduino Example - V1 Holding Register (Modbus IP ESP8266)
  Configure Holding Register (offset 100) with initial value 0xABCD
  You can get or set this holding register
  Original library
  Copyright by André Sarmento Barbosa
  http://github.com/andresarmento/modbus-arduino

  Current version
  (c)2017 Alexander Emelianov (a.m.emelianov@gmail.com)
  https://github.com/emelianov/modbus-esp8266
*/

#ifdef ESP8266
 #include <ESP8266WiFi.h>
#else //ESP32
 #include <WiFi.h>
#endif
#include <ModbusIP_ESP8266.h>
long ts;
// Modbus Registers Offsets
const int V1_HREG = 0;
const int V2_HREG = 1;
const int V3_HREG = 2;
const int I1_HREG = 3;
const int I2_HREG = 4;
const int I3_HREG = 5;
const int P1_HREG = 6;
const int P2_HREG = 7;
const int P3_HREG = 8;
const int CE_HREG = 9;

//ModbusIP object
ModbusIP mb;
  
void setup() {
  Serial.begin(115200);
 
  WiFi.begin("SSID", "Wifi Passwd");
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  mb.server();            //creates the server object

  mb.addHreg(V1_HREG);    // Add all the Registers addres 
  mb.addHreg(V2_HREG);
  mb.addHreg(V3_HREG);
  mb.addHreg(I1_HREG);
  mb.addHreg(I2_HREG);
  mb.addHreg(I3_HREG);
  mb.addHreg(P1_HREG);
  mb.addHreg(P2_HREG);
  mb.addHreg(P3_HREG);
  mb.addHreg(CE_HREG);
  
}
int i =0;
void loop() {
   //Call once inside loop() - all magic here
   mb.task();

   mb.Hreg(V1_HREG,random(110, 120));         // insert a random value over the registers to be read by the client. 
   mb.Hreg(V2_HREG,random(110, 120));
   mb.Hreg(V3_HREG,random(110, 120));
   mb.Hreg(I1_HREG,random(30,40));
   mb.Hreg(I2_HREG,random(30,40));
   mb.Hreg(I3_HREG,random(30,40));
   mb.Hreg(P1_HREG,random(80,90));
   mb.Hreg(P2_HREG,random(80,90));
   mb.Hreg(P3_HREG,random(80,90));
   mb.Hreg(CE_HREG,random(10,20));
  

   
   delay(2000);
}
