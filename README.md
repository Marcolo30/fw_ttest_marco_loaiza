# fw_ttest_Marco_Loaiza

Modbus protocol simulation between 2 slave/server devices  and 1 master/client.



## Goals


* Simulate the 4 devices, each one must return the following variables:
  
    * Voltage L1-N
    * Voltage L1-N
    * Voltage L1-N
    * Current L1
    * Current L2
    * Current L3
    * Cumulative Energy imported
    * Power Factor L1
    * Power Factor L2
    * Power Factor L3
* Send messages to Azure IoT HUB:
    * Json Format
    * including timestamp
    * telemetry and event messages


## Diagram of the solution 

The client provided the following measurement instrumentation:
* 4 x Power Meter SIEMENS PAC3200
* 1 x 8-port Network Switch
* A functional Wi-Fi network
* Other supplies (wires, UTP cable, current transformers, etc)

The proposed diagram using the measuremen instrumentation is the following. 
[Diagram](https://gitlab.com/Marcolo30/fw_ttest_marco_loaiza/-/blob/main/source/images/Diagram.png)

 ## Simulation Diagram 

    MB_DEVICE_ADDR1
    -------------
    |           |
    |  Slave 1  |---<>--+
    |   ESP32   |       |
    -------------       |
                        |
                        |        -------------
                        |        |           |
                        +---<>---|  Master   |
                        |        |    RPI    |
    -------------       |        -------------
    MB_DEVICE_ADDR2     |
    -------------     Network  (WiFi connection)
    |           |       |
    |  Slave 2  |---<>--+
    |   ESP32   |
    -------------


For simulation purpouses, two ESP32 devices were configured as Modbus servers  with the [slave code](https://gitlab.com/Marcolo30/fw_ttest_marco_loaiza/-/blob/main/modbusserver.ino) just by addign the SSID information and password of the network involved. 

And a RPI device runing python to work as the Modbus client device with the [master code](https://gitlab.com/Marcolo30/fw_ttest_marco_loaiza/-/blob/main/ModbusClient.py) configuring IoT Hub CONNECTION_STRING with the device previously created and configured in the Azure IoT Hub. 

A RTOS configuration was tried but there is an error code over the ESP32 that dumps the memory of the device, the code is over review to find the cause or possible solution to deploy the client server over the ESP32 Master device. [RTOS code trial](https://gitlab.com/Marcolo30/fw_ttest_marco_loaiza/-/blob/80fe48280358d448a387017f30cc15f4af698beb/FreeRtosClient.ino)



## General Code description.


To fulfill the project goals, the server code is deployed on the esp32, which randomly generates values of the different variables and saves them in a memory space of the Modbus server accordingly with the protocol it should be saved over the Holding registers that are from address 40001 to 49999. (For simplicity 0 to 10 range was used for the simulation.)

This single code can be deployed and replicated on 4 or more devices to simulate the SIEMENS PAC3200 meter. However, at the time of the project development only two ESP32 devices were available.

On the other hand, threading was used for the client code since it was necessary to connect to different slave IP addresses. 
In our case 2 threads for slave conections were used. Another thread was used to connect to Azure services to send the payloads of the different varables captures from the slaves. 

To exchange information between threads a Queue is used to retain the information while the Azure thread was in holding state. 
Therefore, every time the slave threads capture data, this information is stored in the queue, so that once the azure thread starts running, the process can pull the information from the queue, format the payload message and send it to azure.   


