#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

#ifdef ESP8266
 #include <ESP8266WiFi.h>
#else
 #include <WiFi.h>
#endif
#include <ModbusIP_ESP8266.h>


//ModbusIP object
ModbusIP mb;
const int REG = 0;               // Modbus Hreg Offset
const int LOOP_COUNT = 10;

  uint16_t res0;
  uint16_t res1;
  uint16_t res2;
  uint16_t res3;
  uint16_t res4;
  uint16_t res5;
  uint16_t res6;
  uint16_t res7;
  uint16_t res8;
  uint16_t res9;
  uint8_t show = LOOP_COUNT;

// define two tasks for Slaves readings 
void Lectura_esclavo_1( void *pvParameters );
void Lectura_esclavo_2( void *pvParameters );

// the setup function runs once when you press reset or power the board
void setup() {
  
  // initialize serial communication at 115200 bits per second:
  Serial.begin(115200);
  WiFi.begin("SSID", "PASSWD");
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  mb.client();
  
  
  // Now set up two tasks to run independently.
  xTaskCreatePinnedToCore(
    Lectura_esclavo_1
    ,  "Lectura_esclavo_1"   // A name just for humans
    ,  1024 *2 // This stack size can be checked & adjusted by reading the Stack Highwater
    ,  NULL
    ,  2  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
    ,  NULL 
    ,  ARDUINO_RUNNING_CORE);

  xTaskCreatePinnedToCore(
    Lectura_esclavo_2
    ,  "AnalogReadA3"
    ,  1024 *2  // Stack size
    ,  NULL
    ,  1  // Priority
    ,  NULL 
    ,  ARDUINO_RUNNING_CORE);

  // Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

int ejecutar_proceso(IPAddress remote){¨

/*
  This function map or copy the information of the registers to send it to a queue that can be read by the Azure function that send data to the IoT Hub. 
*/

  if (mb.isConnected(remote)) {   // Check if connection to Modbus Slave is established
      
        mb.readHreg(remote, REG, &res0);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+1, &res1);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+2, &res2);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+3, &res3);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+4, &res4);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+5, &res5);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+6, &res6);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+7, &res7);  // Initiate Read Coil from Modbus Slave
        mb.readHreg(remote, REG+8, &res8);  // Initiate Read Coil from Modbus Slave    
        mb.readHreg(remote, REG+9, &res9);  // Initiate Read Coil from Modbus Slave
     
     } else {
    mb.connect(remote);           // Try to connect if no connection
  }
  mb.task();                      // Common local Modbus task
                                    // Pulling interval
  if (!show--) {  
    
    Serial.println("**********************Datos Esclavo2");     // Display Slave register value one time per second (with default settings)
    Serial.println(res0);
    Serial.println(res1);
    Serial.println(res2);
    Serial.println(res3);
    Serial.println(res4);
    Serial.println(res5);
    Serial.println(res6);
    Serial.println(res7);
    Serial.println(res8);
    Serial.println(res9);

    show = LOOP_COUNT;
  }
    
  };
void loop()
{
  // Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

void Lectura_esclavo_1(void *pvParameters)  // This is a task.
{
  (void) pvParameters;




  // IP Slave1 .
  IPAddress remote(192, 168, 1, 107); 

  
  
  while (1) // A Task shall never return or exit.
  {
    ejecutar_proceso(remote);
    
    vTaskDelay(2000);  // one tick delay (15ms) in between reads for stability
  }
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Lectura_esclavo_2(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

/*
*/


  // IP Slave2 .
  IPAddress remote(192, 168, 1, 104); 

  
  while (1) // A Task shall never return or exit.
  {
   ejecutar_proceso(remote); 
    vTaskDelay(1000);  // one tick delay (15ms) in between reads for stability
  }
}
