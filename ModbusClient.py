# -*- coding: utf-8 -*-
"""
Created on Wed Sep  7 15:41:19 2022

@author: Maloa
"""
from pymodbus.client.sync import ModbusTcpClient
from datetime import datetime, time
from random import randint,random
import time
import threading
from datetime import datetime
from azure.iot.device import IoTHubDeviceClient, MethodResponse,Message
import queue



CONNECTION_STRING = "{deviceConnectionString}"


def slave1():        
   """ This Modbus function connect to slave 1 and read the registers on it.
        then upload the info into a queue to pass the information to the send thread
   """ 
   host = '192.168.1.107'  
   port = 502
   reg=0
   client = ModbusTcpClient(host, port)
   valor=[0,0,0,0,0,0,0,0,0,0,""]
   
   while True:
        client.connect()
        result= client.read_holding_registers(reg, 10)
        
        
        valor[0]=result.registers[0]
        valor[1]=result.registers[1]
        valor[2]=result.registers[2]
        valor[3]=result.registers[3]
        valor[4]=result.registers[4]
        valor[5]=result.registers[5]
        valor[6]=result.registers[6]
        valor[7]=result.registers[7]
        valor[8]=result.registers[8]
        valor[9]=result.registers[9]
        valor[10]="Foundry"
        datos.put(valor)
      
        time.sleep(10)
        
def slave2():
    """
    This Modbus function connect to slave 2 and read the registers on it.
    then upload the info into a queue to pass the information to the send thread
    """
   host = '192.168.1.104'  #ip of slave 2
   port = 502
   reg=0
   client = ModbusTcpClient(host, port)
   valor=[0,0,0,0,0,0,0,0,0,0,""]
   
   while True:
        client.connect()
        result = client.read_holding_registers(reg, 10)
        
      
       
        
        valor[0]=result.registers[0]
        valor[1]=result.registers[1]
        valor[2]=result.registers[2]
        valor[3]=result.registers[3]
        valor[4]=result.registers[4]
        valor[5]=result.registers[5]
        valor[6]=result.registers[6]
        valor[7]=result.registers[7]
        valor[8]=result.registers[8]
        valor[9]=result.registers[9]
        valor[10]="Molding"
        
        datos.put(valor)
        time.sleep(10)
        

def send():
    """
    This function capture data from queues where diferent slaves information is, format the message 
    and send it to Azure IoThub.
    It also send an event message if it founds that I1,I2 or I3 exceed the treshold value. 
    """
    CONNECTION_STRING = "HostName=Medidoreshub.azure-devices.net;DeviceId=ModbusClientDevice;SharedAccessKey=BX6QpYSKrXPZtw2MZQKgXxv6NNe1hBUhScopLIdqddc="
    MSG_TXT = '{{"Timestamp": {Timestamp},"Area": {Area} ,"V1": {V1} ,"V2": {V2} ,"V3": {V3},"I1": {I1} ,"I2": {I2} ,"I3": {I3},"P1": {P1} ,"P2": {P2} ,"P3": {P3},"EA": {EA}}}'
    MSG_EVENT ='{{"Timestamp": {Timestamp},"Area": {Area} ,"Payload": {Payload}}}'

    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)
    treshold=30;

    while True:
         
        if datos.empty():
            
            time.sleep(5)
        else:
            a=datos.get()
            
            V1=a[0]
            V2=a[1]
            V3=a[2]
            I1=a[3]
            I2=a[4]
            I3=a[5]
            P1=a[6]
            P2=a[7]
            P3=a[8]
            EA=a[9]
            Area=a[10]
                
            
            now = datetime.now()
            Timestamp = now.strftime("%Y%m%d:%H:%M:%S")
            msg_txt_formatted = MSG_TXT.format(Timestamp=Timestamp,Area=Area,V1=V1,V2=V2,V3=V3,I1=I1,I2=I2,I3=I3,P1=P1,P2=P2,P3=P3,EA=EA)
            message = Message(msg_txt_formatted)

            
            print( "Sending message: {}".format(message) )
            client.send_message(message)
            print ( "Message successfully sent" )
            
            if (I1>=treshold | I2>=treshold | I3>=treshold):
                print("-----------------------------------------------------------------------")
                Payload="Mensaje de Alerta, la corriente sobrepasa el valor treshold "
                msg_txt_formatted = MSG_EVENT.format(Timestamp=Timestamp,Area=Area,Payload=Payload)
                message = Message(msg_txt_formatted)    
                    
                    
            
            
            time.sleep(5)
        


    
datos = queue.Queue(maxsize=100)

    
t1 = threading.Thread(target=slave1)
t2 = threading.Thread(target=slave2)
t3 = threading.Thread(target=send)


t1.start()
t2.start()
t3.start()

t1.join()
t2.join()
t3.join()
